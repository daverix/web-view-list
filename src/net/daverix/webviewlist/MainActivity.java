package net.daverix.webviewlist;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

public class MainActivity extends Activity implements TabListener, OnPageChangeListener {
	private ViewPager mPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		mPager = (ViewPager) findViewById(R.id.pager);
		
		ActionBar ab = getActionBar();
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		ab.addTab(ab.newTab().setText("WebView rows").setTabListener(this));
		ab.addTab(ab.newTab().setText("TextView (html)").setTabListener(this));
		ab.addTab(ab.newTab().setText("Webpage").setTabListener(this));
		ab.addTab(ab.newTab().setText("Android (xml)").setTabListener(this));
		
		mPager.setAdapter(new ViewPagerFragmentAdapter(getFragmentManager()));
		mPager.setOnPageChangeListener(this);
	}

	private class ViewPagerFragmentAdapter extends FragmentPagerAdapter {

		public ViewPagerFragmentAdapter(FragmentManager fm) {
			super(fm);
			
		}

		@Override
		public Fragment getItem(int pos) {
			switch (pos) {
			case 0:
				return new WebViewListFragment();
			case 1:
				return new TextViewListFragment();
			case 2:
				return new WebViewFragment();
			case 3:
				return new DataListFragment();
			default:
				return null;
			}
		}

		@Override
		public int getCount() {
			return 4;
		}
		
	}
	
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		
	}

	@Override
	public void onPageSelected(int position) {
		getActionBar().setSelectedNavigationItem(position);
	}
}
