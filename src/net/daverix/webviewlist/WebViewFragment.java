package net.daverix.webviewlist;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class WebViewFragment extends Fragment {
	private WebView mWebView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_web, null);
		
		mWebView = (WebView) v.findViewById(R.id.web);
		
		return v;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		new GetItemsTask().execute();
	}
	
	private class GetItemsTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			return getHtml();
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			mWebView.loadDataWithBaseURL("file:///android_asset/", result, "text/html", "UTF-8", null);
		}
	}
	
	private String getHtml() {
		StringBuilder builder = new StringBuilder();
		builder.append("<!Doctype html>");
		builder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />");
		
		Random r = new Random();
		for(int i=0;i<Config.ROWS_TO_LOAD;i++) {
			builder.append("<div style=\"border-bottom: 1px solid #ccc\">");
				builder.append("<div class=\"title\">This is the title of order ");
					builder.append(i+1);
				builder.append("</div>");
				
				builder.append("<div class=\"time\">");
					builder.append(SimpleDateFormat.getTimeInstance().format(new Date(System.currentTimeMillis() - 60000*i)));
				builder.append("</div>");
				
				builder.append("<div class=\"status\">");
					builder.append(r.nextInt(10));
				builder.append("</div>");
				builder.append("<div style=\"clear:both;\"></div>");
			builder.append("</div>");
		}
		
		return builder.toString();
	}
}
