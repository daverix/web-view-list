package net.daverix.webviewlist;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.app.ListFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;

public class WebViewListFragment extends ListFragment {
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		new GetItemsTask().execute();
	}
	
	private class GetItemsTask extends AsyncTask<Void, Void, List<String>> {

		@Override
		protected List<String> doInBackground(Void... params) {
			List<String> items = new ArrayList<String>();
			Random r = new Random();
			for(int i=0;i<Config.ROWS_TO_LOAD;i++) {
				StringBuilder builder = new StringBuilder();
				builder.append("<!Doctype html>");
				builder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />");
				builder.append("<div>");
					builder.append("<div class=\"title\">This is the title of order ");
						builder.append(i+1);
					builder.append("</div>");
					
					builder.append("<div class=\"time\">");
						builder.append(SimpleDateFormat.getTimeInstance().format(new Date(System.currentTimeMillis() - 60000*i)));
					builder.append("</div>");
					
					builder.append("<div class=\"status\">");
						builder.append(r.nextInt(10));
					builder.append("</div>");
					builder.append("<div style=\"clear:both;\">");
				builder.append("</div>");
				items.add(builder.toString());
			}
			
			return items;
		}
		
		@Override
		protected void onPostExecute(List<String> result) {
			super.onPostExecute(result);
			
			setListAdapter(new WebViewListAdapter(result));
		}
		
	}
	
	private class WebViewListAdapter extends BaseAdapter {
		private List<String> _items;
		
		public WebViewListAdapter(List<String> items) {
			_items = items;
		}
		
		@Override
		public int getCount() {
			return _items.size();
		}

		@Override
		public String getItem(int position) {
			return _items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			WebView view = (WebView) convertView;
			if(view == null) {
				view = (WebView) LayoutInflater.from(getActivity()).inflate(R.layout.list_item_textview, null);
			}
			view.loadDataWithBaseURL("file:///android_asset/", getItem(position), "text/html", "UTF-8", null);
			
			return view;
		}
	}
}
