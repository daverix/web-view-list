package net.daverix.webviewlist;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.app.ListFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TextViewListFragment extends ListFragment {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	
		new GetItemsTask().execute();
	}
	
	private class GetItemsTask extends AsyncTask<Void, Void, List<Spanned>> {

		@Override
		protected List<Spanned> doInBackground(Void... params) {
			List<Spanned> items = new ArrayList<Spanned>();
			Random r = new Random();
			for(int i=0;i<Config.ROWS_TO_LOAD;i++) {
				StringBuilder builder = new StringBuilder();
				builder.append("<h3>This is the title of order ");
					builder.append(i+1);
				builder.append("</h3><br>");
				
				builder.append("<p><font color=\"green\">");
					builder.append(SimpleDateFormat.getTimeInstance().format(new Date(System.currentTimeMillis() - 60000*i)));
				builder.append("</font></p>");
				
				builder.append("<h2>");
					builder.append(r.nextInt(10));
				builder.append("</h2>");
				
				String html = builder.toString();
				items.add(Html.fromHtml(html));
			}
			return items;
		}
		
		@Override
		protected void onPostExecute(List<Spanned> result) {
			super.onPostExecute(result);
			
			setListAdapter(new TextViewListAdapter(result));
		}
		
	}

	private class TextViewListAdapter extends BaseAdapter {
		private List<Spanned> _items;
		
		public TextViewListAdapter(List<Spanned> items) {
			_items = items;
		}
		
		@Override
		public int getCount() {
			return _items.size();
		}

		@Override
		public Spanned getItem(int position) {
			return _items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView view = (TextView) convertView;
			if(view == null) {
				view = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.list_item_text, null);
			}
			view.setText(getItem(position));
			
			return view;
		}	
	}
	
}
