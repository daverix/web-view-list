package net.daverix.webviewlist;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.app.ListFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DataListFragment extends ListFragment {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		new GetItemsTask().execute();
	}
	
	private class GetItemsTask extends AsyncTask<Void, Void, List<Row>> {

		@Override
		protected List<Row> doInBackground(Void... params) {
			List<Row> items = new ArrayList<Row>();
			Random r = new Random();
			for(int i=0;i<Config.ROWS_TO_LOAD;i++) {
				String title = "This is the title of order " + (i+1);
				String time = SimpleDateFormat.getTimeInstance().format(new Date(System.currentTimeMillis() - 60000*i));
				String status = String.valueOf(r.nextInt(10));
				items.add(new Row(title, time, status));
			}
			return items;
		}
		
		@Override
		protected void onPostExecute(List<Row> result) {
			super.onPostExecute(result);
			
			setListAdapter(new XmlListAdapter(result));
		}
		
	}
		
	private class Row {
		private String mTitle, mTime, mStatus;
		
		public Row(String title, String time, String status) {
			mTitle = title;
			mTime = time;
			mStatus = status;
		}

		/**
		 * @return the title
		 */
		public String getTitle() {
			return mTitle;
		}

		/**
		 * @return the time
		 */
		public String getTime() {
			return mTime;
		}

		/**
		 * @return the status
		 */
		public String getStatus() {
			return mStatus;
		}
	}
	
	private class XmlListAdapter extends BaseAdapter {
		private List<Row> _items;
		
		public XmlListAdapter(List<Row> items) {
			_items = items;
		}
		
		@Override
		public int getCount() {
			return _items.size();
		}

		@Override
		public Row getItem(int position) {
			return _items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			
			if(convertView == null) {
				convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item, null);
				holder = new ViewHolder();
				holder.title = (TextView) convertView.findViewById(R.id.textTitle);
				holder.time = (TextView) convertView.findViewById(R.id.textTime);
				holder.status = (TextView) convertView.findViewById(R.id.textStatus);
				
				convertView.setTag(holder);
			}
			else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			Row r = getItem(position);
			holder.title.setText(r.getTitle());
			holder.time.setText(r.getTime());
			holder.status.setText(r.getStatus());
			
			return convertView;
		}	
	}
	
	private static class ViewHolder {
		TextView title;
		TextView time;
		TextView status;
	}
}
